// Put images url here or can use local images from folder image.
$.backstretch([
    "images/logo.png"
  , "https://upload.wikimedia.org/wikipedia/commons/thumb/8/8f/Example_image.svg/600px-Example_image.svg.png"

  // Duration var change to how long you want the image to stay.
], {duration: 13000, fade: 750});
