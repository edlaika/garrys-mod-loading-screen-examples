# Garry-s-Mod-loading-screen-examples
These are some loading screens examples for your server!

Please note:
All of the content go to their respective owners.

Due to a high demand here is the code I use for my loading screens. 

The basic one allows you to load the image with all resolutions. No coding experience needed just download paste your image and youtube link!

The advanced one allows you to play random unlimited music make sure to have format .ogg also allows you to display multi images.

Download the source code and play around how you like.

I don't recommend advanced loading screen for beginner's.

The code includes comments inside just follow the instructions.

Server side you can use PHP:

```
#!PHP
<?php
$communityid = $_GET["steamid"];
$mapname = $_GET["mapname"];
$authserver = bcsub($communityid, '76561197960265728') & 1;
$authid = (bcsub($communityid, '76561197960265728')-$authserver)/2;
$steamid = "STEAM_0:$authserver:$authid";

echo "Welcome to my server!<br>";
echo "Your Community ID is $communityid<br>";
echo "Your SteamID is $steamid<br>";
echo "The current map is $mapname<br>";
echo "Enjoy your stay!";
?>

```